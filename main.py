import json
import utils
from sympy import Symbol, true, symbols

if __name__ == '__main__':
    a = Symbol('a')
    b = Symbol('b')
    p = Symbol('p')
    t = Symbol('t')
    r = Symbol('r')
    d1 = utils.DefeasibleImplication(a, b, 0)
    d2 = utils.DefeasibleImplication(b, r, 0)
    d3 = utils.DefeasibleImplication(true, a | b, 0)
    d4 = utils.DefeasibleImplication(r, p, 0)
    d5 = utils.DefeasibleImplication(a, ~p, 0)
    dk = utils.DefeasibleKnowledgeBase([d1, d2, d4, d5])
    dk2 = utils.DefeasibleKnowledgeBase([d3, d2, d1, d3])
    #print(dk)
   # print("models" + str(list(satisfiable(d1.get_materialization() & d2.get_materialization() & d3.get_materialization()))))
    #dk.z_rank = dk.z_ranking()
    rank = dk.r_0_ranking()
    print(dk)
    print()
    j_rank = dk.system_j_ranking()
    z_rank = dk.z_ranking()
    dk.rank_J = j_rank
    rank_alg = dk.get_j_ranking_alg(j_rank)
    xs = rank_alg.domain
    print("domain:", xs)
    print(str(dict(j_rank)))
    print(rank_alg)
    x0, x1, x2 = symbols('x_0,x_1,x_2')
    kb = utils.KnowledgeBase([a | t, b, p])
    kb2 = utils.KnowledgeBase([b, p, t])
    kb3 = utils.KnowledgeBase([a])
    print()
    print(str(kb3), "U", str(dk), "|~_J", str(b), "is", dk.system_j(kb3, b, j_rank, rank_alg))
    print(str(kb3), "U", str(dk), "|~_J", str(~p), "is", dk.system_j(kb3, ~p, j_rank, rank_alg))
    print(str(kb3), "U", str(dk), "|~_J", str(r), "is", dk.system_j(kb3, r, j_rank, rank_alg))
    print()
    print(str(kb), "U", str(dk), "|~_J", str(r), "is", dk.system_j(kb, r, j_rank, rank_alg))
    print()
    r_alg = utils.RankingSemantics([a, b, p, r], j_rank, rank_alg)
    print(r_alg)
    print(str(z_rank))

    # print(dk.is_consistent(s))
    # print("System Z: ")
    # for key, value in dk.z_rank.items():
    #     print(key, ":  ", end="")
    #     for val in value:
    #         print(val)
    # print("Z System: ")
    # for key, value in dk.rank_z.items():
    #     print(key, value)

