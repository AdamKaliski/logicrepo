from builtins import staticmethod
import copy
from sympy.logic.inference import satisfiable, pl_true, literal_symbol
from sympy import Symbol, true, false
from sympy.core import oo
from collections import defaultdict
from functools import cmp_to_key


def get_distinct_symbols(symbs):
    res = []
    for symb in symbs:
        if type(symb) == int:
            if symb not in res:
                res.append(symb)
                continue
        for sym in symb.free_symbols:
            if sym not in res:
                res.append(sym)
    return res


def get_all_models(lang):
    res = [{lang[0]: True}, {lang[0]: False}]
    for l in lang[1:]:
        res1 = copy.deepcopy(res)
        res2 = copy.deepcopy(res)
        final_res = []
        for mod in res1:
            mod[l] = True
            final_res.append(mod)
        for mod in res2:
            mod[l] = False
            final_res.append(mod)
        res = final_res
    return res


def get_z_rank_material(z_rank):
    res = defaultdict(list)
    for key, values in z_rank.items():
        for val in values:
            res[key].append(val.get_materialization())
    return res


def get_conjunction(list_of_statements):
    conj = list_of_statements[0]
    for rule in list_of_statements[1:]:
        conj = conj & rule
    return conj


def convert_dis_to_mat(list_of_dis):
    res = []
    for r in list_of_dis:
        res.append(r.get_materialization())
    return res


def get_ranks(ranking, formula):
    ranks = []
    for key, vals in ranking.items():
        for val in vals:
            if pl_true(formula, val):
                if key not in ranks:
                    ranks.append(key)
    return ranks if len(ranks) > 0 else oo


def is_infinite(term):
    atoms = term.atoms()
    if oo in atoms:
        return oo
    if -oo in atoms:
        return -oo
    return False


class RankingSemantics:
    def __init__(self, domain, ranking, rank_alg):
        if domain:
            self.domain = domain
        else:
            self.domain = []
        if ranking:
            self.ranking = ranking
        else:
            self.ranking = {}
        if rank_alg:
            self.ranking_algebra = rank_alg
        else:
            self.ranking_algebra = RankingAlgebra(None, None)

    def __str__(self):
        st = ""
        # sort the key, value pairs by the preference relation here for printing
        pairs = [(key, val) for key, val in self.ranking.items()]
        keys = [key for key, val in pairs]
        print("*****")
        print(self.ranking_algebra)
        print(keys)
        keys = self.ranking_algebra.sort_keys(keys, 0, len(keys)-1)
        print(keys)
        print("*****")
        for key, val in self.ranking.items():
            st += str(key) + ": " + str(val) + "\n"
        for pref in self.ranking_algebra.preferences:
            for d in self.domain:
                if d in pref:
                    st += str(pref)
        return st

    def update_domain(self, d_impl):
        props = d_impl.get_props()
        for prop in props:
            if prop not in self.domain:
                self.domain.append(prop)


class RankingAlgebra:
    def __init__(self, domain, preferences):
        if domain:
            self.domain = get_distinct_symbols(domain)
        else:
            self.domain = []
        if preferences:
            self.preferences = preferences
        else:
            self.preferences = []
        self.domain.append(oo) if oo not in self.domain else self.domain
        self.domain.append(0) if 0 not in self.domain else self.domain
        # self.domain.append(-oo) if -oo not in self.domain else self.domain
        for x in self.domain:
            if x != oo and (x, oo) not in self.preferences:
                self.add_preference(x, oo)
            if x != 0 and (0, x) not in self.preferences:
                self.add_preference(0, x)

            #     if -x != -oo and (-x, oo) not in self.preferences:
            #         self.add_preference(-x, oo)
            # if x != -oo and (-oo, x) not in self.preferences:
            #     self.add_preference(-oo, x)
            #     if -x != -oo and (-oo, -x) not in self.preferences:
            #         self.add_preference(-oo, -x)
            # System J+
            # if x != 0 and x != -oo and (0, x) not in self.preferences:
            #     self.add_preference(0, x)
            #     if (-x, 0) not in self.preferences:
            #         self.add_preference(-x, 0)

    def __str__(self):
        return str(self.domain) + "\n" + str(self.preferences)

    def add_preference(self, x, y):
        self.preferences.append((x, y))
        self.add_transitive_preferences(x, y)
        if x != oo and x != 0:
            self.add_additive_preferences(x)
        if y != oo and y != 0:
            self.add_additive_preferences(y)


    def get_preferred(self, li):
        res = li[0]
        for sym in li[1:]:
            c = self.comparison(sym, res)
            if c == 1:
                res = sym
        return res

    def get_preference_relation(self, x, less_than=True):
        res = []
        for pref in self.domain:
            if pref == x:
                continue
            if less_than:
                if (x, pref) in self.preferences:
                    res.append(pref)
            else:
                if (pref, x) in self.preferences:
                    res.append(pref)
        return res

    def update_preferences(self, l1, l2):
        x = self.get_preferred(l1)
        y = self.get_preferred(l2)
        if x not in self.domain or y not in self.domain:
            return False
        if (x, y) not in self.preferences:
            self.preferences.append((x, y))
            if x != oo and (x, oo) not in self.preferences:
                self.preferences.append((x, oo))
            if y != oo and (y, oo) not in self.preferences:
                self.preferences.append((y, oo))
            # self.preferences.append((-y, -x))
        else:
            return True
        self.add_transitive_preferences(x, y)
        if x != oo and x != 0:
            self.add_additive_preferences(x)
        if y != oo and y != 0:
            self.add_additive_preferences(y)
        """ 
        y_cons = self.get_preference_relation(y)
        x_cons = self.get_preference_relation(x, less_than=False)
        for xcon in x_cons:
            for ycon in y_cons:
                if (xcon, ycon) not in self.preferences and xcon != ycon:
                    self.preferences.append((xcon, ycon))
        negy_cons = self.get_preference_relation(-y)
        negx_cons = self.get_preference_relation(-x, less_than=False)
        for negxcon in negx_cons:
            for negycon in negy_cons:
                if (negycon, negxcon) not in self.preferences and negycon != negxcon:
                    self.preferences.append((negycon, negxcon)) 
        """

    def add_additive_preferences(self, x):
        for d in self.domain:
            if x != d and d != 0 and d != oo:
                if (x, x+d) not in self.preferences:
                    self.preferences.append((x, x+d))
                    if x+d != oo:
                        self.preferences.append((x + d, oo))
                    self.add_transitive_preferences(x, x+d)
                # if (-x-d, -x) not in self.preferences:
                #     self.preferences.append((-x-d, -x))

    def add_transitive_preferences(self, x, y):
        for pref in self.preferences:
            if pref[0] == y and x != pref[1]:  # anything more than y is more than x
                if (x, pref[1]) not in self.preferences:
                    self.preferences.append((x, pref[1]))
                # if (-pref[1], -x) not in self.preferences:
                #     self.preferences.append((-pref[1], -x))
            if pref[1] == x and y != pref[0]:  # anything less than x is less than y
                if (pref[0], y) not in self.preferences:
                    self.preferences.append((pref[0], y))
                # if (-y, -pref[0]) not in self.preferences:
                #     self.preferences.append((-y, -pref[0]))

    def less_than(self, x, y):
        return (x, y) in self.preferences

    def compare(self, x, y):
        if self.less_than(x, y):
            return -1
        elif self.less_than(y, x):
            return 1
        else:
            return 0

    def greater_than(self, x, y):
        return (y, x) in self.preferences

    def comparison(self, x, y):
        if (y == 0 or x == 0) or (len(x.atoms()) == 1 and len(y.atoms()) == 1):
            count = 0
            if self.less_than(x, y):
                count += 1
            if self.greater_than(x, y):
                count += 2
            return count
        elif len(x.atoms()) > 1:
            temp = x - y
            temp_inf = is_infinite(temp)
            if not temp_inf:
                res = self.comparison(temp, 0)
                return res
            else:
                return self.comparison(temp_inf, 0)
        elif len(y.atoms()) > 1:
            temp = y - x
            temp_inf = is_infinite(temp)
            if not temp_inf:
                res = self.comparison(0, temp)
                return res
            else:
                return self.comparison(temp_inf, 0)

    def update_domain(self, element):
        if type(element) == list:
            self.domain += element
        elif type(element) == Symbol:
            self.domain.append(element)

    def get_min(self, ranks):
        res = []
        for r in ranks:
            is_min = True
            for k in ranks:
                if r == k:
                    continue
                if self.comparison(k, r) == 1:
                    is_min = False
                    break
            if is_min:
                res.append(r)
        return res

    def sort_keys(self, keys, lo, hi):
        return sorted(keys, key=cmp_to_key(self.compare))


class DefeasibleImplication:
    def __init__(self, antecedent, consequent, delta):
        self.ante = antecedent
        self.cons = consequent
        self.delt = delta

    def __eq__(self, other):
        if self is None and other is None:
            return True
        elif self is None:
            return False
        elif other is None:
            return False
        return self.ante == other.ante and self.cons == other.cons and self.delt == other.delt

    def __str__(self):
        return str(self.ante) + " ~>_" + str(self.delt) + " " + str(self.cons)

    def __hash__(self):
        return hash(repr(self))

    def get_materialization(self):
        return self.ante >> self.cons

    def get_conj_neg(self):
        return self.ante & ~self.cons

    def get_conj_pos(self):
        return self.ante & self.cons

    def get_props(self):
        return [self.ante, self.cons]


class DefeasibleKnowledgeBase:
    def __init__(self, DIs):
        self.Delta = DIs
        self.z_rank = None
        self.kappa_rank = None
        self.rank_z = None
        self.rank_J = None
        self.j_ranking_algebra = None

    def __eq__(self, other):
        return self.set_minus(other) == [] and other.set_minus(self) == []

    def __str__(self):
        res = "{"
        for r in self.Delta:
            res = res + str(r) + ", "
        return res[:-2] + "}"

    def get_props(self):
        l = []
        for r in self.Delta:
            antecedent = list(r.ante.atoms())
            consequent = list(r.cons.atoms())
            total = antecedent + consequent
            for t in total:
                if t not in l:
                    l.append(t)
        return l

    @staticmethod
    def query(query, ranking):
        if ranking is None:
            return "Error: please specify a valid entailment relation"

        for key, value in ranking.items():
            ranking[key] = convert_dis_to_mat(value)

        concatenated_knowledge = [query.get_materialization()]
        for key, value in ranking.items():
            concatenated_knowledge += value
        conj_conc_knowledge = get_conjunction(concatenated_knowledge)
        res = list(satisfiable(conj_conc_knowledge, all_models=True))
        count = 0
        while not res[0]:
            print(ranking)
            ranking.pop(count)
            count += 1
            if len(ranking) == 0:
                break
            concatenated_knowledge = [query.get_materialization()]
            for key, value in ranking.items():
                concatenated_knowledge += value
            conj_conc_knowledge = get_conjunction(concatenated_knowledge)
            res = list(satisfiable(conj_conc_knowledge, all_models=True))
        return res

    def set_minus(self, other):
        res = []
        for d in self.Delta:
            eq = False
            for r in other.Delta:
                if d == r:
                    eq = True
                    break
            if not eq:
                res.append(d)
        return res

    @staticmethod
    def kappa_rank_models(z_rank, models):
        kappa = defaultdict(list)
        for model in models:
            rank = 0
            for key, val in z_rank.items():
                for a in val:
                    r = a.get_materialization()
                    if pl_true(r.args[0] & ~r.args[1], model):
                        rank = key if key > rank else rank
            rank += 1
            kappa[rank].append(model)
        return kappa

    def get_material_conjunction(self):
        res = []
        for r in self.Delta:
            res.append(r.get_materialization())
        conj = res[0]
        for r in res[1:]:
            conj = conj & r
        return conj

    def add_statement(self, DI):
        self.Delta.append(DI)

    def r_0_ranking(self):
        language = self.get_props()
        models = get_all_models(language)
        res = defaultdict(list)
        res[0] = models
        return res

    @staticmethod
    def kappa_pi_ranking_query(formula, ranking, weak):
        for rank, values in ranking:
            conj = get_conjunction(values)
            if list(satisfiable(conj & formula)):
                return

    def shift(self, rank, prop, a):
        language = self.get_props()
        res = defaultdict(list)
        for r, vals in rank.items():
            for val in vals:
                if pl_true(prop, val, deep=True):
                    res[r+a].append(val)
                else:
                    res[r].append(val)
        return res

    def system_j_ranking(self):
        rank = self.r_0_ranking()
        count = 0
        for d in self.Delta:
            rank = self.shift(rank, d.get_conj_neg(), Symbol('x_'+str(count)))
            count += 1
        return rank

    @staticmethod
    def j_rank_sat(rule, rank, rank_algebra):
        pos_rank, neg_rank = get_ranks(rank, rule.get_conj_pos()), get_ranks(rank, rule.get_conj_neg())
        if neg_rank == oo:
            return True
        elif pos_rank == oo:
            return False
        p_rank = rank_algebra.get_min(pos_rank)
        n_rank = rank_algebra.get_min(neg_rank)
        res = rank_algebra.comparison(p_rank[0], n_rank[0])
        return res

    def get_j_ranking_alg(self, ranking):
        vals = ranking.keys()
        r_a = RankingAlgebra(list(vals), None)
        for d in self.Delta:
            pos_rank = get_ranks(ranking, d.get_conj_pos())
            neg_rank = get_ranks(ranking, d.get_conj_neg())
            r_a.update_preferences(pos_rank, neg_rank)
        self.j_ranking_algebra = r_a
        return r_a

    def system_j(self, kb, query, ranking, ranking_alg):
        diffs = {}
        for delta in self.Delta:
            diff = self.j_rank_sat(delta, ranking, ranking_alg)
            if not diff > 0 or not diff:
                return False
            diffs[delta] = diff
        default_query = DefeasibleImplication(kb.get_conjunction(), query, 0)  # delta is hardcoded 0
        res = self.j_rank_sat(default_query, ranking, ranking_alg)
        if not res:
            return False
        if res == 0:
            return 0
        return res == 1

    def system_jz(self, kb, query, ranking, ranking_alg):

        return

    def z_ranking(self):
        delta_0, delta_prime = self.get_tolerated()
        rz_plus = DefeasibleKnowledgeBase([])
        z_rank = defaultdict(list)
        rankz = {}
        for d in delta_0.Delta:
            z_rank[d.delt].append(d)
            rz_plus.add_statement(d)
            rankz[d] = d.delt
        while not (rz_plus == self):
            delta_p = delta_prime
            delta_plus, delta_prime = delta_p.get_tolerated()
            min_rule_rank = 999999999
            for r in delta_plus.Delta:
                delta_p_conj = delta_p.get_material_conjunction()
                omega = satisfiable((r.ante & r.cons) & delta_p_conj, all_models=True)
                kappa_ranks = self.kappa_rank_models(z_rank, omega)
                min_rank = sorted(kappa_ranks.keys())[0] + r.delt
                z_rank[min_rank].append(r)
                rankz[r] = min_rank
                if min_rank < min_rule_rank:
                    min_rule_rank = min_rank
            for rule in z_rank[min_rule_rank]:
                rz_plus.add_statement(rule)
        self.z_rank = z_rank
        self.rank_z = rankz
        return z_rank

    def tolerates(self, r):
        conj = self.get_material_conjunction()
        if not satisfiable((r.ante & r.cons) & conj):
            return False
        return True

    def get_tolerated(self):
        delta_0 = []
        delta_1 = []
        for d in self.Delta:
            copy = list(self.Delta)
            copy.remove(d)
            if DefeasibleKnowledgeBase(copy).tolerates(d):
                delta_0.append(d)
            else:
                delta_1.append(d)
        return DefeasibleKnowledgeBase(delta_0), DefeasibleKnowledgeBase(delta_1)

    def is_consistent(self, query=None):
        return satisfiable(self.get_material_conjunction() & query) is not False

    def relevant_closure(self, query):

        return


class KnowledgeBase:
    def __init__(self, formulas):
        if formulas is None:
            self.formulas = []
        else:
            self.formulas = formulas

    def __eq__(self, other):
        return self.set_minus(other) == [] and other.set_minus(self) == []

    def __str__(self):
        res = "{"
        for r in self.formulas:
            res = res + str(r)
        return res + "}"

    def set_minus(self, other):
        res = []
        for f in self.formulas:
            if f not in other.formulas:
                res.append(f)
        return res

    def get_conjunction(self):
        res = self.formulas[0]
        for f in self.formulas[1:]:
            res = res & f
        return res
